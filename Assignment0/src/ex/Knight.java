package ex;

public class Knight {
	int baseHP;
	 int wp;
	 
	 Knight(int baseHP, int wp){
		 if(baseHP >= 99 && baseHP <=999)
		 {
			 if(wp == 0 || wp == 1)
			 {
				 this.baseHP = baseHP;
				 this.wp = wp;
			 }
		 }
	 }
	 
	 void setBaseHP(int baseHP) {
		 this.baseHP = baseHP;
	 }
	 int getBaseHP() {
		 return baseHP;
	 }
	 

	 int getWp() {
		 return wp;
	 }
	 
	 
	 int getRealHP() {
		if (wp == 1)
			 return baseHP;
		return baseHP/10;
	 }
}
