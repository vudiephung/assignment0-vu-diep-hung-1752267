package ex;

public class Warrior {
	int baseHP;
	int wp;
	 
	 Warrior(int baseHP, int wp){
		 if(baseHP >= 1 && baseHP <=888)
		 {
			 if(wp == 0 || wp == 1)
			 {
				 this.baseHP = baseHP;
				 this.wp = wp;
			 }
		 }
	 }
	 
	 void setBaseHP(int baseHP) {
		 this.baseHP = baseHP;
	 }
	 int getBaseHP() {
		 return baseHP;
	 }
	 
	 int getWp() {
		 return wp;
	 }
	 
	 int getRealHP() {
		if (wp == 1)
			 return baseHP;
		return baseHP/10;
	 }
}
